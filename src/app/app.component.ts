import { Component } from '@angular/core';
import { MovieDataService } from './movie-data.service';
import { Movie } from "app/movie";
import { Subject } from "rxjs/Subject";

@Component({
  selector: 'app-root',
  templateUrl : 'app.component.html',
  styleUrls: [],
  providers : [MovieDataService]
})
export class AppComponent {
  private selectedMovie : Movie;
  private movies : Array<Movie> = new Array<Movie>();
  private searchTerm$ = new Subject<string>();
  private searchMovie$ = new Subject<string>();

  constructor(private movieService : MovieDataService){
        this.movieService.search(this.searchTerm$)
          .subscribe(results => {
            this.movies = results.Search;
          });
        this.movieService.searchMovie(this.searchMovie$)
          .subscribe(results => {
            this.selectedMovie = results;
          });       
  }
}
