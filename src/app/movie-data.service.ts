import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/catch';

@Injectable()
export class MovieDataService {

  constructor(private http : Http) { }

  search(terms: Observable<string>) {
    return terms.debounceTime(400)
      .distinctUntilChanged()
      .switchMap(term => this.searchEntries(term, false));
  }

  searchMovie(terms: Observable<string>) {
    return terms.debounceTime(1000)
      .distinctUntilChanged()
      .switchMap(term => this.searchEntries(term, true));
  }

  searchEntries(searchTerm, isDetails : boolean) {
    return this.http
        .get('http://www.omdbapi.com/?apikey=8559af41&'+(isDetails ? '&plot=full&t' : 's')+'='+searchTerm)
        .map(res => res.json());
  }
}
